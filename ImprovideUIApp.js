import * as React from 'react';
import { Text, 
         View, 
         StyleSheet, 
         Image, 
         ScrollView,
         FlatList, 
         Linking,
         StatusBar,
         TouchableOpacity
        } from 'react-native';
import { Constants } from 'expo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

// You can import from local files
import AssetExample from './components/AssetExample';

// or any pure javascript modules available in npm
import { Card } from 'react-native-paper';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    var jsonData = require('./assets/Info.json');
    this.state = {
      data: jsonData,
    };
  }
  render() {
    return (
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
        <Text style={styles.app_name}>TOP10 IMDB MOVIE</Text>
        
        <FlatList 
        data={this.state.data}
        renderItem={({item}) =>
          <View>
            <MovieView title={item.title}
                       image={item.image}
                       url={item.url}/>
          </View>
        }
        />
      </ScrollView>
    );
  }
}

class MovieTitle extends React.Component {
  render() {
   return (
     <Text style={styles.movie__title}>{this.props.title}</Text>
   );
  }
}

class MoviePoster extends React.Component {
  render() {
    return (
      <Image
        style={styles.movie__poster} 
        source={{uri: this.props.image}}/>
    );
  }
}

class MovieDetailButton extends React.Component {
  
  handlePress(url) {
    Linking.openURL(url);
  }

  render() {
    return (
      <TouchableOpacity
        style={styles.movie__button}
        onPress={() => this.handlePress(this.props.url)}>
        <Text style={styles.button__label}>More Detail</Text>
      </TouchableOpacity>
    )
  }
}

class MovieView extends React.Component {
  render () {
    return (
      <View  style={styles.container__movie}>
        <MovieTitle title={this.props.title}/>
        <MoviePoster image={this.props.image}/>
        <MovieDetailButton url={this.props.url}/>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  contentContainer: {
    paddingVertical: 20,
    top: hp('1%'),
  },
  app_name: {
    flex: 1,
    alignSelf: 'center',
    fontSize: 24,
    fontStyle: 'italic' ,
    fontWeight: 'bold',
  },
  container__movie: {
    flex: 1,
    backgroundColor: '#302424',
    width: wp('90%'),
    height: 'auto',
    padding: 35,
    position: 'relative',
    top: 20,
    marginBottom: 20,
    alignSelf: 'center',
    borderWidth: 1,
    borderRadius: 10,
  },
  movie__title: {
    flex: 1,
    position: 'absolute',
    top: 5,
    alignSelf: 'center',
    fontSize: 14,
    paddingTop: 5,
    color: '#A70A0A',
    fontWeight: 'bold',
  },
  movie__poster: {
    position: 'relative',
    alignSelf: 'center',
    width: '100%',
    height: hp('50%'),
    marginBottom: 10,
    marginTop: 15,
    borderRadius: 10,
  },
  movie__button: {
    backgroundColor: '#A70A0A',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: wp('53,14%'),
    height: hp('5,02%'),
    borderRadius: 10,
  },
  button__label: {
    alignSelf: 'center',
    color: 'white',
    fontWeight: 'bold',
  }
});