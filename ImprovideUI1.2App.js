import * as React from 'react';
import { Text, 
         View, 
         StyleSheet, 
         Image, 
         ScrollView,
         FlatList, 
         Linking,
         StatusBar,
         TouchableOpacity,
         ImageBackground
        } from 'react-native';
import { Constants } from 'expo';

// Library for responsive screen
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

// You can import from local files
import AssetExample from './components/AssetExample';

// or any pure javascript modules available in npm
import { Card } from 'react-native-paper';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    var jsonData = require('./assets/Info.json');
    this.state = {
      data: jsonData,
    };
  }
  render() {
    return (
      <View style={styles.screen_container}>
        <Text style={styles.app_name}>TOP10 IMDB MOVIE</Text>
        <FlatList 
          data={this.state.data}
          renderItem={({item}) =>
          <View>
            <MovieView title={item.title}
                       image={item.image}
                       imdb={item.imdb}
                       url={item.url}/>
          </View>
        }
        />
      </View>
    );
  }
}

class MovieTitle extends React.Component {
  render() {
   return (
     <Text style={styles.movie__title}>{this.props.title}</Text>
   );
  }
}

class MovieDesc extends React.Component {
  render() {
    return (
      <View style={styles.movie__decs}>
        <MovieTitle title={this.props.title}/>
        <MovieDetailButton url={this.props.url}/>
      </View>
    );
  }
}

class MovieIMDBScore extends React.Component {
  render() {
    return (
      <TouchableOpacity style={styles.movie__imdb}
                        onPress={() => this.props.onPress()}>
        <Text style={styles.imdb__score}>{this.props.imdb}</Text>
      </TouchableOpacity>
    )
  }
}

class MovieDetailButton extends React.Component {
  
  handlePress(url) {
    Linking.openURL(url);
  }

  render() {
    return (
      <TouchableOpacity
        style={styles.movie__button}
        onPress={() => this.handlePress(this.props.url)}>
        <Text style={styles.button__label}>More Detail</Text>
      </TouchableOpacity>
    )
  }
}

class MovieView extends React.Component {
  constructor(props){
    super(props);
    this.state= {
      blur: 0,
      pressStatus: false,
    };
  }
  showMovieDesc(pressStatus) {
    if(pressStatus == false) {
      this.setState({blur: 2, pressStatus: true});
    }
    else {
      this.setState({blur: 0, pressStatus:false});
    }
  }
  render () {
    return (
      <ImageBackground source={{uri: this.props.image}}  
                      style={styles.container__movie}
                      blurRadius={this.state.blur}>
        <MovieIMDBScore 
          imdb={this.props.imdb}
          onPress={() => this.showMovieDesc(this.state.pressStatus)}/>
        {this.state.pressStatus ? 
          <MovieDesc title={this.props.title}
                     desc={this.props.desc}
                     url={this.props.url}/>
          : null}
      </ImageBackground>
    )
  }
}


const styles = StyleSheet.create({
  screen_container: {
    backgroundColor: '#0D0D0D',
    flex: 1,
    width: wp('100%'),
    height: hp('100%'),
    paddingTop: Constants.statusBarHeight,
  },
  app_name: {
    backgroundColor: '#BF0413',
    width: wp('100%'),
    textAlign: 'center',
    fontSize: 24,
    fontStyle: 'italic' ,
    fontWeight: 'bold',
    color: 'white',
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
  },
  container__movie: {
    width: wp('80%'),
    height: hp('70%'),
    padding: 35,
    position: 'relative',
    top: 20,
    marginBottom: 20,
    alignSelf: 'center',
  },
  movie__imdb: {
    backgroundColor: 'rgba(225, 225, 225, 0.7)',
    top: hp('22%'),
    alignSelf: 'center',
    justifyContent: 'center',
    width: 70,
    height: 70,
    borderRadius: 70/2,
    borderColor: 'white',
  },
  imdb__score: {
    textAlign: 'center',
    margin: 'auto',
    color: 'transparen',
    fontWeight: "bold",
    fontSize: 24,
  },
  movie__decs: {
    display: 'flex',
  },
  movie__title: {
    position: 'absolute',
    textAlign: 'center',
    top: 5,
    alignSelf: 'center',
    fontSize: 16,
    paddingTop: 5,
    color: 'white',
    fontWeight: 'bold',
  },
  movie__button: {
    position: "relative",
    borderBottomWidth: 2,
    borderColor: 'red',
    justifyContent: 'center',
    alignSelf: 'center',
    width: wp('40%'),
    height: hp('5,02%'),
    borderRadius: 10,
    top: hp('27%'),
  },
  button__label: {
    alignSelf: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  }
});