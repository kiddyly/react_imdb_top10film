import * as React from 'react';
import { Text, View, StyleSheet, Button, Image, ScrollView, Linking } from 'react-native';
import { Constants } from 'expo';

// You can import from local files
import AssetExample from './components/AssetExample';

// or any pure javascript modules available in npm
import { Card } from 'react-native-paper';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    const jsonData = getJson();
    this.state = {
      data: jsonData,
    };
  }
  render() {
    var itemArray = []
    // This for loop create MovieView and inint data for each
    for (let i = 0; i<this.state.data.length; i++) {
      var item = (
        <MovieView style={styles.container}
        title = {this.state.data[i].title}
        image = {this.state.data[i].image}
        url = {this.state.data[i].url}/>
      );
      itemArray[i] = item;
    }
    return (
      <ScrollView>
        {itemArray}
      </ScrollView>
    );
  }
}

class MovieTitle extends React.Component {
  render() {
   return (
     <Text style={styles.movie__title}>{this.props.title}</Text>
   );
  }
}

class MoviePoster extends React.Component {
  render() {
    return (
      <Image
        style={styles.movie__poster} 
        source={{uri: this.props.image}}/>
    );
  }
}

class MovieDetailButton extends React.Component {
  
  handlePress(link) {
    Linking.openURL(link);
    // alert('What the fuck going on?');
  }

  render() {
    return (
      <Button
        title='More Detail'
        onPress={() => this.handlePress(this.props.link)}
      />
    )
  }
}

class MovieView extends React.Component {
  render () {
    return (
      // Some thing a button and text
      <View>
        <MovieTitle title={this.props.title}/>
        <MoviePoster image={this.props.image}/>
        <MovieDetailButton link={this.props.url}/>
      </View>
    )
  }
}

// get data from Json
function getJson() {
  var data = require('./assets/Info.json');
  return data;
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    top: 20,
  },
  movie__title: {
    margin: 'auto',
  },
  movie__poster: {
    marginLeft: 20,
    width: 300,
    height: 445,
  },
});

