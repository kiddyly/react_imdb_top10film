import * as React from 'react';
import { Text, 
         View, 
         StyleSheet, 
         Button, Image, 
         ScrollView,
         FlatList, 
         Linking 
        } from 'react-native';
import { Constants } from 'expo';

// You can import from local files
import AssetExample from './components/AssetExample';

// or any pure javascript modules available in npm
import { Card } from 'react-native-paper';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    var jsonData = require('./assets/Info.json');
    this.state = {
      data: jsonData,
    };
  }
  render() {
    return (
      <ScrollView>
        <FlatList 
        data={this.state.data}
        renderItem={({item}) =>
          <View>
            <MovieTitle title={item.title}/>
            <MoviePoster image={item.image}/>
            <MovieDetailButton url={item.url}/>
          </View>
        }
        />
      </ScrollView>
    );
  }
}

class MovieTitle extends React.Component {
  render() {
   return (
     <Text style={styles.movie__title}>{this.props.title}</Text>
   );
  }
}

class MoviePoster extends React.Component {
  render() {
    return (
      <Image
        style={styles.movie__poster} 
        source={{uri: this.props.image}}/>
    );
  }
}

class MovieDetailButton extends React.Component {
  
  handlePress(url) {
    Linking.openURL(url);
    // alert('What the fuck going on?');
  }

  render() {
    return (
      <Button
        title='More Detail'
        onPress={() => this.handlePress(this.props.url)}
      />
    )
  }
}

class MovieView extends React.Component {
  render () {
    return (
      // Some thing a button and text
      <View>
        <MovieTitle title={this.props.title}/>
        <MoviePoster image={this.props.image}/>
        <MovieDetailButton url={this.props.url}/>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    position: 'relative',
    top: 20,
  },
  movie__title: {
    margin: 'auto',
  },
  movie__poster: {
    marginLeft: 20,
    width: 300,
    height: 445,
  },
});

